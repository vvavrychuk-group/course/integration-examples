﻿using GraphQL;

namespace GraphQLCRUDServer
{
    class Author : ICloneable
    {
        public long Id { get; init; }
        public string Name { get; set; }
        public List<Book> Books { get; private set; } = new();

        public object Clone()
        {
            return new Author()
            {
                Id = Id,
                Name = Name,
                Books = Books.Select(book => (Book)book.Clone()).ToList()
            };
        }
    }

    class Book : ICloneable
    {
        public long Id { get; init; }
        public string Title { get; set; }
        public List<Author> Authors { get; init; } = new();

        public object Clone()
        {
            return new Book()
            {
                Id = Id,
                Title = Title,
                Authors = Authors.Select(author => (Author)author.Clone()).ToList()
            };
        }
    }

    class BookInput
    {
        public long Id { get; init; }
        public string Title { get; init; }
        public List<long> Authors { get; init; }
    }

    class LibraryRepository
    {
        long id = 1L << 32;

        List<Author> authors = new();
        List<Book> books;

        long GetId()
        {
            return id++;
        }

        Author FindAuthor(long id)
        {
            var author = authors.Find(author => author.Id == id);
            if (author == null)
            {
                throw new ExecutionError("Invalid author id");
            }
            return author;
        }

        Book GetBook(BookInput bookInput)
        {
            var authors = bookInput.Authors.Select(author => FindAuthor(author)).ToList();
            return new Book()
            {
                Id = bookInput.Id != 0 ? bookInput.Id : GetId(),
                Title = bookInput.Title,
                Authors = authors,
            };
        }

        public LibraryRepository()
        {
            books = new(new Book[] { new Book { Id = id++, Title = "The Art of Computer Programming" } });
        }

        public List<Author> GetAuthors()
        {
            lock (this)
            {
                return authors.Select(author => (Author)author.Clone()).ToList();
            }
        }

        public Author CreateAuthor(Author authorInput)
        {
            lock (this)
            {
                var author = new Author() { Id = id++, Name = authorInput.Name };
                authors.Add(author);
                return author;
            }
        }

        public Author UpdateAuthor(Author authorInput)
        {
            lock (this)
            {
                var authorIndex = authors.FindIndex(author => author.Id == authorInput.Id);
                if (authorIndex == -1)
                {
                    throw new ExecutionError("Invalid author id");
                }
                var author = authors[authorIndex];
                author.Name = authorInput.Name;
                return author;
            }
        }

        public Author DeleteAuthor(long id)
        {
            lock (this)
            {
                if (books.Any(book => book.Authors.Any(author => author.Id == id)))
                {
                    throw new ExecutionError("Author is in use");
                }
                var author = authors.Find(author => author.Id == id);
                if (author == null)
                {
                    throw new ExecutionError("Invalid author id");
                }
                authors.Remove(author);
                return author;
            }
        }

        public List<Book> GetBooks()
        {
            lock (this)
            {
                return books.Select(book => (Book)book.Clone()).ToList();
            }
        }

        public Book CreateBook(BookInput bookInput)
        {
            lock (this)
            {
                var book = GetBook(bookInput);
                books.Add(book);
                return book;
            }
        }

        public Book UpdateBook(BookInput bookInput)
        {
            lock (this)
            {
                var book = books.FindIndex(book => book.Id == bookInput.Id);
                if (book == -1)
                {
                    throw new ExecutionError("Invalid book id");
                }
                books[book] = GetBook(bookInput);
                return books[book];
            }
        }

        public Book DeleteBook(long id)
        {
            lock (this)
            {
                var book = books.Find(book => book.Id == id);
                if (book == null)
                {
                    throw new ExecutionError("Invalid book id");
                }
                books.Remove(book);
                return book;
            }
        }
    }
}
