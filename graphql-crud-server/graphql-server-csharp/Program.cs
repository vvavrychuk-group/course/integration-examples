using GraphQL;
using GraphQL.SystemTextJson;
using GraphQL.Transport;
using GraphQL.Types;

namespace GraphQLCRUDServer
{
    class Query
    {
        readonly LibraryRepository repository;

        public Query(LibraryRepository repository) => this.repository = repository;

        public List<Author> Authors => repository.GetAuthors();

        public List<Book> Books => repository.GetBooks();
    }

    class Mutation
    {
        readonly LibraryRepository repository;

        public Mutation(LibraryRepository repository) => this.repository = repository;

        public Author CreateAuthor(Author author) => repository.CreateAuthor(author);

        public Author UpdateAuthor(Author author) => repository.UpdateAuthor(author);

        public Author DeleteAuthor(long author) => repository.DeleteAuthor(author);

        public Book CreateBook(BookInput book) => repository.CreateBook(book);

        public Book UpdateBook(BookInput book) => repository.UpdateBook(book);

        public Book DeleteBook(long book) => repository.DeleteBook(book);
    }


    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddSingleton<LibraryRepository>();
            builder.Services.AddSingleton<Query>();
            builder.Services.AddSingleton<Mutation>();
            var app = builder.Build();

            var schema = Schema.For(@"
              type Author {
                id: ID!
                name: String!
                books: [Book]
              }

              type Book {
                id: ID!
                title: String!
                authors: [Author]
              }

              type Query {
                authors: [Author]
                books: [Book]
              }

              input AuthorInput {
                id: ID
                name: String!
              }

              input BookInput {
                id: ID
                title: String!
                authors: [ID]
              }

              type Mutation {
                createAuthor(author: AuthorInput): Author
                updateAuthor(author: AuthorInput): Author
                deleteAuthor(author: ID): Author

                createBook(book: BookInput): Book
                updateBook(book: BookInput): Book
                deleteBook(book: ID): Book
              }
            ", _ =>
            {
                _.ServiceProvider = app.Services;
                _.Types.Include<Query>();
                _.Types.Include<Mutation>();
            });

            app.MapPost("/graphql", async (HttpContext context) =>
            {
                var requestJson = await new StreamReader(context.Request.Body).ReadToEndAsync();
                var request = new GraphQLSerializer().Deserialize<GraphQLRequest>(requestJson);
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = 200;
                await context.Response.WriteAsync(await schema.ExecuteAsync(_ =>
                {
                    _.Query = request.Query;
                    _.OperationName = request.OperationName;
                    _.Variables = request.Variables;
                    _.ThrowOnUnhandledException = true;
                }));
            });

            app.Run();
        }
    }
}
