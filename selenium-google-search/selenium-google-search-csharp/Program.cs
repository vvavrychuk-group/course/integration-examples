﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

class GoogleHomePage
{
    readonly WebDriver driver;

    public GoogleHomePage(WebDriver driver)
    {
        this.driver = driver;
    }

    public GoogleSearchResults Search(string text)
    {
        new Actions(driver).SendKeys(text).SendKeys(Keys.Return).Perform();
        return new GoogleSearchResults(driver);
    }
}

class GoogleSearchResults
{
    readonly WebDriver driver;

    public GoogleSearchResults(WebDriver driver)
    {
        this.driver = driver;
    }

    public List<string> GetTitles()
    {
        var titles = new List<string>();
        foreach (var element in driver.FindElement(By.Id("rso")).FindElements(By.TagName("h3")))
        {
            titles.Add(element.Text);
        }
        return titles;
    }

    public GooglePaginator GetPaginator()
    {
        foreach (var navigation in driver.FindElements(By.CssSelector("[role = 'navigation']")))
        {
            // TODO замінити цикли на XPath selector
            foreach (var backwardForwardSpan in navigation.FindElements(By.TagName("span")))
            {
                if ((backwardForwardSpan.Text == "Назад") || (backwardForwardSpan.Text == "Уперед"))
                {
                    return new GooglePaginator(navigation);
                }
            }
        }
        return null;
    }
}

class GooglePaginator
{
    readonly IWebElement element;

    public GooglePaginator(IWebElement element)
    {
        this.element = element;
    }

    public string? GetPaginatorPage(int num)
    {
        var links = element.FindElements(By.TagName("td"))[num].FindElements(By.TagName("a"));
        if (links.Count == 0)
        {
            return null;
        }
        if (links.Count != 1)
        {
            throw new Exception();
        }
        return links[0].GetAttribute("href");
    }
}

class Program
{
    public static void Main(string[] args)
    {
        var driver = new ChromeDriver();
        driver.Navigate().GoToUrl("https://www.google.com");

        var search = new GoogleHomePage(driver).Search("погода");
        for (int pageNum = 1; ; pageNum++)
        {
            if (pageNum != 1)
            {
                var page = search.GetPaginator().GetPaginatorPage(pageNum);
                if (page != null)
                {
                    driver.Navigate().GoToUrl(page);
                }
                else
                {
                    break;
                }
            }
            foreach (var title in search.GetTitles())
            {
                Console.WriteLine(title);
            }
        }

        driver.Quit();
    }
}
